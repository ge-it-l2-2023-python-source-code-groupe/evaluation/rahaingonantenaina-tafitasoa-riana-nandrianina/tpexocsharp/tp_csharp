using System;

using Function;



namespace Dé
{
    class Main_program

    {

        internal static void Program()
        {
            Console.WriteLine("");
            Console.WriteLine("""
            __________Jeux de Dé__________
            """);

            bool program = true;
            while (program != false)
            {
                Console.WriteLine(
                   """
                 __________________________________
                 |1) Nouvelle partie               |
                 |2) Voir l'historique des parties |
                 |3) Quitter le programme          |
                 |_________________________________|
                """
                   );
                Console.Write("Votre choix  \n  >>>>>> : ");
                int userChoice = Int32.Parse(Console.ReadLine() ?? "");

                switch (userChoice)
                {
                    case 1:
                        Console.WriteLine("____________Nouvelle partie__________");
                        Function.Newgame.NewPlayer();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.WriteLine("____________HISTORIQUE_____________");
                        Console.WriteLine("-----le temps de partie-----");
                        Function.Newgame.History();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("______VOUS ALLEZ QUITTER_____");
                        Console.ReadKey();
                        Console.Clear();
                        program = false;

                        break;
                    default:
                        Console.WriteLine("UNE ERREUR S'EST PRODUIT ");
                        break;
                }
            }



        }

    }
}






















