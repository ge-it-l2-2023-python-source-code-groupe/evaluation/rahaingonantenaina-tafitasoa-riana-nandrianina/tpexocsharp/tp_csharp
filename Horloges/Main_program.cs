using System;
using System.Security.Cryptography;
using Alarm;
using ClockApp;

namespace Horloges
{

    class Main_program
    {
        internal static void Program()
        {
            int choice;
             Console.WriteLine("");
             Console.WriteLine("""
             _________Horloge_________
            """);

            do
            {
                DisplayMenu();
                choice = GetChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        AlarmApp alarmApp = new AlarmApp();
                        alarmApp.Run();
                        break;
                    case 2:
                        ClockApp.ClockApp.RunClockApp();
                        break;

                    default:
                        break;
                }
            } while (choice != 3);

            void DisplayMenu()

            {
           
            Console.WriteLine("");
            Console.WriteLine("""
            
                OPTIONS


             _____________
             |1) Alarme  |
             |2) Horloge |
             |3) Quitter |
             |___________|


             Choisissez parmis les options
            """);
            Console.Write("Votre options : ");
            
            }
            
            int GetChoice()
            {
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine(" CHOIX INVALIDE");
                    Console.Write(" --->Votre choix : ");
                }
                return choice;
            }



        }
    }
}
