using System;
using System.Collections.Generic;


namespace ClockApp
{
    class ClockApp
    {
        public static void RunClockApp(){
            ClockApp clockApp=new ClockApp();
            clockApp.Run();
        }
        private List<Clock> clockList = new List<Clock>();

        public void Run()
        {
            int choice;

            do
            {
                DisplaySubMenu();
                choice = GetSubMenuChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        ViewActiveClocks();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 2:
                        AddClock();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 3:
                        Console.WriteLine("> Retour au menu principal <");
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    default:
                        Console.WriteLine("invalide. Veuillez réessayer ");
                        break;
                }

            } while (choice != 3);
        }

        private void DisplaySubMenu()

        {
            Console.WriteLine("""
            
             Choisisssez 
            
             1) Voir les Horloges actives 
             2) Ajouter une horloge 
             3) Retour au menu principale 
            
            
            """);
            Console.Write("---- Choix:  ");
        
        }

        private int GetSubMenuChoice()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine(" INVALIDE. Veuillez entrer un nombre ");
                Console.Write("---- Choix ==  ");
               
            }
            return choice;
        }

        private void ViewActiveClocks()
        {
            
            Console.WriteLine("Liste des horloges :");
            DisplayCurrentTime();
            foreach (var clock in clockList)
            {
                Console.WriteLine(clock.ToString());
            }

            
             
        }

        private void DisplayCurrentTime()
        {
            // Afficher le temps actuel (peut être mis à jour en temps réel)
            Console.WriteLine($"""
                      Antananrivo
            {DateTime.Now.ToString("HH:mm:ss")}
            {DateTime.Now.ToString("ddd dd MMM")}
            """);
            
        }

        private void AddClock()
        {
            Console.WriteLine("_____Choisissez parmi les  villes ______");
            Console.Write("""
                Moscou 
                Bakou
                Arizona
                Séoul
                Brasil
                Hawaii
                Canada
                
                __veuillez bien choisir__
                
                votre choix---->
                """);
            string city = Console.ReadLine() ?? "";

            Clock newClock = new Clock(city);
            clockList.Add(newClock);

            Console.WriteLine("______ Votre horloge a bien été enregistré____");
            
        }
    }

    class Clock
    {
        public string City { get; set; }
        public int TimeDifference { get; set; } 

        public Clock(string city)
        {
            City = city;
            
            
            TimeDifference = GetTimeDifference(city);
        }

        private static int GetTimeDifference(string city)
        {
            return city.ToLower() switch
            {
                "moscou" => 3,
                "Bakou" => 4,
                "Arizona" => -6,
                "Séoul" =>9,
                "Brasil" =>-3,
                "Hawaii" =>10,
                "Canada" =>-7,
                _ => 0,
            };
        }

        public override string ToString()

        {
            DateTime localTime=DateTime.UtcNow.AddHours(TimeDifference);
            string timeString= localTime.ToString("HH:mm");
            return $"""
            Autre horloge 
            {City} - {timeString} (GMT{(TimeDifference >= 0 ? "+" : "")}{TimeDifference} / {localTime}) 
             
                         __Entrer pour revenir____
            """;
        }
    }

}