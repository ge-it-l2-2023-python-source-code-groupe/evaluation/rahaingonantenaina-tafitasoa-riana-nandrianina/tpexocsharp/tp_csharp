using System;
using Cercle_class;
using Point_class;

namespace Cercles
{
    class Main_program
    {


        internal static void Program()

        {
            Console.WriteLine("");
             Console.WriteLine("""
            -----------Tp_N_2_Csarp-----------
            """);
            Console.Write(" Donner l'abscisse du centre : ");
            double x = double.Parse(Console.ReadLine());
            Console.Write(" entrez l'ordonné du centre : ");
            double y = double.Parse(Console.ReadLine());
            Console.Write(" entrez le rayon : ");
            double r = double.Parse(Console.ReadLine());

            //création d'un cercle
            Console.WriteLine("----------Cercle-------------");
            Cercle cercle = new Cercle(x, y, r);
            cercle.Display();
            cercle.Perimetre();
            cercle.Surface();

            //input point à verifier
            Console.WriteLine("  --Donner un point à verifier--  ");
            Console.Write("  valeur X : ");
            int monx = int.Parse(Console.ReadLine() ?? "");
            Console.Write("  valeur Y : ");
            int mony = int.Parse(Console.ReadLine() ?? "");

            //creation d'un point
            Point point = new Point(monx, mony);
            point.View();

            //verification de l'appartenance du point 
            point.VerificationPoint(monx, mony, x, y, r);

        }
    }
}