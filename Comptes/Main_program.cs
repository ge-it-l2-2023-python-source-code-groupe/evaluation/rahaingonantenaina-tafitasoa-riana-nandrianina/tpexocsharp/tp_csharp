using System;
using Client_class;
using Compte_class;
namespace Comptes
{


    class Main_program
    {


        internal static void Program()
        {
            string? cin, firstName, lastName, tel;
            float? somme;
            Console.WriteLine("""
            
            ________Compte Bancaire_______

            """);

            // #Crée un  Compte Client 1
         
            Console.WriteLine("Compte  N°1");
            Console.WriteLine("veuillez le formulaire :");
            Console.Write($"Saisissez Le CIN= ");
            cin = Console.ReadLine();
            Console.Write($"Entrer le Nom= ");
            firstName = Console.ReadLine();
            Console.Write($"Entrer le Prénom= ");
            lastName = Console.ReadLine();
            Console.Write($"Entrer le numero= ");
            tel = Console.ReadLine();

            Compte cpt1 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt1.Resumer()}");


            // # Crediter & Debiter Compte Client 1

            
            Console.Write($"Entrer le montant à déposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Crediter((float)somme);
            Console.WriteLine($" _____ Opération effectuée{cpt1.Resumer()} ____");
            //Debiter le compte 1
            Console.Write($"Entrer le montant à retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme);
            Console.Write($" ____Opération effectuée{cpt1.Resumer()}____ ");
            
            Console.ReadKey();
            // # Crée Compte Client 2
            Console.WriteLine("____________Compte N°2__________");
            Console.Write($"\n\n\nCompte 2:\nEntrer Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Entrer Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Entrer Le Prénom: ");
            lastName = Console.ReadLine();
            Console.Write($"Entrer Le numéro de télephone: ");
            tel = Console.ReadLine();

            Compte cpt2 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Détails du compte:{cpt2.Resumer()}");


            Console.ReadKey();
            Console.WriteLine("                       ");
            //Crediter cpt2 -> cpt1
            Console.WriteLine($":: Crediter le compte {cpt2.Code} à partir du compte {cpt1.Code} :: ");
            Console.Write($"Donner le montant à déposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt2.Crediter((float)somme, cpt1);
            Console.Write($" ____Opération bien effectuée____ ");
             Console.WriteLine("                     ");
            
            
            //Debiter cpt1 -> cpt2
            Console.WriteLine($"Débiter le compte {cpt1.Code} et créditer le compte {cpt2.Code}");
            Console.Write($"Donner le montant à retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme, cpt2);
            Console.WriteLine($"== Opération bien effectuée == ");

             
              Console.WriteLine("                                ");
               Console.WriteLine("______________ RESUME____________");
            Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
            Console.Write($"\n\n\n{Compte.NombreCompteCree()}");
            Console.ReadKey();
        }
    }
}